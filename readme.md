---
title: Simulateur de netlist
author: Lucas Baudin
date: 8 novembre 2015
---

Format d'entrée des inputs
--------------------------

On prend les données initiales sur l'entrée standards. Une liste d'input est sur une seule ligne, et est constitué de 0 et de 1. voir les fichiers en `.test` dans le dossier `tests`.

Par exemple, pour lancer l'exécution du `nadder` 2 bits, on utilise :

```
./launcher.byte -n 4 tests/nadder.net < tests/nadder.net.test2
```

Gestion des RAM et des registres
--------------------------------

Tout est fait en une seule passe. Si on a une équation de la forme `a = reg(b)`,
alors on ajoute une dépendance de `a` par rapport à `b` : ainsi, lorsque la
simulation a lieu, on calcule la valeur de `a` et ensuite celle de `b`, ce qui
donne bien que `a` a la valeur de `b` du cycle précédent.

Cependant, bien que cette méthode ne nécessite qu'une seule passe, elle ne
permet pas d'effectuer des simulations sur des circuits de la forme :
```
x = reg y;
y = reg x;
```

puisque cela va créer un cycle dans le graphe de dépendances.

Les instructions de RAM (et ROM) sont séparées en deux, une pour la lecture et
une pour l'écriture.

Exécution
---------

Sur de grosses simulations, la première partie (calcul du graphe de dépendances,
ordonnancement, etc…) prend beaucoup plus de temps. Par exemple, sur un
additionneur 2000 bits, le pré-calcul prend environ 60 secondes, alors que la
simulation prend un temps négligeable.
