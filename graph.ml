exception Cycle
type mark = NotVisited | InProgress | Visited

type 'a graph =
    { g_nodes : ('a, 'a node) Hashtbl.t }
and 'a node = {
  n_label : 'a;
  mutable n_mark : mark;
  mutable n_link_to : 'a node list;
  mutable n_linked_by : 'a node list;
}

let mk_graph () = { g_nodes = Hashtbl.create 50000 }

let add_node g x =
  try
  	let a = Hashtbl.find g.g_nodes x in a
  with Not_found -> begin
  	let n = { n_label = x; n_mark = NotVisited; n_link_to = []; n_linked_by = [] } in
  	Hashtbl.replace g.g_nodes x n; n end

let node_for_label g =  Hashtbl.find g.g_nodes

let add_edge_real g n1 n2 =
  n1.n_link_to <- n2::n1.n_link_to;
  n2.n_linked_by <- n1::n2.n_linked_by


let add_edge g id1 id2 =
  let n1 = node_for_label g id1 in
  let n2 = node_for_label g id2 in
  add_edge_real g n1 n2

let clear_marks g =
  Hashtbl.iter (fun x n -> n.n_mark <- NotVisited) g.g_nodes

let find_roots g =
  Hashtbl.fold (fun x n l -> if n.n_linked_by = [] then n::l else l) g.g_nodes []

let has_cycle g =
	clear_marks g;
	let rec browse_graph = function
	| [] -> false
	| t::q -> begin match t.n_mark with
		| InProgress -> true
		| NotVisited -> (t.n_mark <- InProgress; browse_graph t.n_link_to || (t.n_mark <- Visited; browse_graph q))
		| Visited -> false
		end
	in
	let hashtbl_to_list x n l = n::l in
	browse_graph (Hashtbl.fold hashtbl_to_list g.g_nodes [])

let topological g =
	clear_marks g;
	let stack = ref [] in
	let i = ref 0 in
	let rec tri_topo x =
		x.n_mark <- InProgress;
		incr i;
		let parc n = if n.n_mark = NotVisited then tri_topo n
		in
		List.iter parc x.n_link_to;
		x.n_mark <- Visited;
		stack := (x.n_label)::(!stack);
	in
	List.iter tri_topo (find_roots g);
	!stack

