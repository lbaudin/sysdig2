open Graphics

let () = open_graph " 800x600"
let out_count = 8

exception BadFormat

let _ =
	begin
	let a = ref "" in
	while (!a <> "-") do
		a := read_line ();
		print_string !a;
		print_string "\n";
	done;
	while true do
		let last_line_read = ref "" in
		let write_enable = ref false in
		let write_adress = ref 0 in
		let write_data = ref 0 in
		for i = 0 to 8 do
			last_line_read := read_line ();
			if i = 5 then
				write_enable := !last_line_read = "1";
			if i = 6 then
				write_adress := int_of_string ( "0x" ^ !last_line_read);
			if i = 7 then
				write_data := int_of_string ( "0x" ^ !last_line_read);
		done;
		if !last_line_read <> "-" then
			raise BadFormat;

		if !write_enable then
			begin
			set_color !write_data;
			(*plot (!write_adress mod 0x10) (!write_adress/0x10);*)
			fill_rect (10*(!write_adress mod 0x100)) (10*(!write_adress/0x100 mod 0x100)) 10 10;
			end
	done;
	end
