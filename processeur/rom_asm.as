mov $0x1, %r12
mov $0x0, %rcx
mov %r12, (%rcx)
mov $0, %rsp
mov (%rsp), %rsp

# Elle va pas être grosse cette pile. Ça tombe bien, on fait une montre…
mov $0x8ff, %rsp
mov $3800, %r12
	mov $0x40d, %rcx
	mov $0xff0000, %rbx
	mov $0x40d, %rdx
	call line_horiz

	mov $0x60d, %rcx
	mov $0xffff00, %rbx
	mov $0x60d, %rdx
	call line_horiz
	
	mov $0x419, %rcx
	mov $0x0000ff, %rbx
	mov $0x419, %rdx
	call line_horiz

	mov $0x619, %rcx
	mov $0x00ffff, %rbx
	mov $0x619, %rdx
	call line_horiz


debut:
	mov %r15, %rcx
	# supposons qu'on est cadencé à 1000hz
	mov $1000, %rax
	call divide
	mov %rax, %rcx
	
	mov $3600, %rax
	call divide
	mov %rcx, %r14

	# affiche de l'heure
	mov %rax, %rcx
	mov $10, %rax
	call divide

	mov %rcx, %r11

	mov %rsp, %rbx
	mov $32, %rsp
	mov (%rsp), %rsp
	mov %rsp, %rcx
	mov %rbx, %rsp
	cmp %rcx, %rax
	je second_number_hour

	mov $32, %rcx
	mov %rax, (%rcx)

	mov $0x202, %rcx
	call draw_number

second_number_hour:
	
	mov %r11, %rax
	
	mov %rsp, %rbx
	mov $40, %rsp
	mov (%rsp), %rsp
	mov %rsp, %rcx
	mov %rbx, %rsp
	cmp %rcx, %rax
	je first_number_minute

	mov $40, %rcx
	mov %rax, (%rcx)

	mov $0x207, %rcx
	call draw_number

first_number_minute:
	mov %r14, %rcx
	mov $60, %rax
	call divide

	mov %rcx, %r14

	# affiche de l'heure
	mov %rax, %rcx
	mov $10, %rax
	call divide

	mov %rcx, %r11

	mov %rsp, %rbx
	mov $0, %rsp
	mov (%rsp), %rsp
	mov %rsp, %rcx
	mov %rbx, %rsp
	cmp %rcx, %rax
	je second_number_minute

	mov $0, %rcx
	mov %rax, (%rcx)

	mov $0x20e, %rcx
	call draw_number

second_number_minute:
	
	mov %r11, %rax
	
	mov %rsp, %rbx
	mov $8, %rsp
	mov (%rsp), %rsp
	mov %rsp, %rcx
	mov %rbx, %rsp
	cmp %rcx, %rax
	je first_number_sec

	mov $8, %rcx
	mov %rax, (%rcx)

	mov $0x213, %rcx
	call draw_number


first_number_sec:
	# minutes
	mov %r14, %rcx
	mov $10, %rax
	
	call divide

	mov %rcx, %r11
	
	mov %rsp, %rbx
	mov $16, %rsp
	mov (%rsp), %rsp
	mov %rsp, %rcx
	mov %rbx, %rsp
	cmp %rcx, %rax
	je second_number_sec

	mov $16, %rcx
	mov %rax, (%rcx)

	mov $0x21a, %rcx
	call draw_number

second_number_sec:
	mov %r11, %rax
	mov %rsp, %rbx
	mov $24, %rsp
	mov (%rsp), %rsp
	mov %rsp, %rcx
	mov %rbx, %rsp
	cmp %rcx, %rax
	je third_number

	mov $24, %rcx
	mov %rax, (%rcx)

	mov $0x21f, %rcx
	call draw_number


third_number:
	
	# on boucle
	mov $0x1, %rax
	add %rax, %r12
	
	jmp debut

# %rcx = %rcx mod %rax, %rax = %rcx / %rax
divide:
	mov %rax, %rdx
	mov $0x1, %r8
	mov $0x0, %rax
divide_loop:
	cmp  %rdx, %rcx
	jge divide_out
	add %r8, %rax
	sub %rdx, %rcx
	jmp divide_loop
divide_out:
	mov (%rsp), %rdx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rdx




# coord dans %rcx, nombre < 10 dans rax
# utilise r8, r9, r10
draw_number:

	mov %rax, %rdx
	mov %rcx, %r10
	
	mov $0x0, %rax
	cmp %rax, %rdx
	je do0
	mov $0x1, %rax
	cmp %rax, %rdx
	je do1
	
	mov $0x2, %rax
	cmp %rax, %rdx
	je do2

	mov $0x3, %rax
	cmp %rax, %rdx
	je do3
	
	mov $0x4, %rax
	cmp %rax, %rdx
	je do4

	mov $0x5, %rax
	cmp %rax, %rdx
	je do5

	mov $0x6, %rax
	cmp %rax, %rdx
	je do6

	mov $0x7, %rax
	cmp %rax, %rdx
	je do7

	mov $0x8, %rax
	cmp %rax, %rdx
	je do8

	mov $0x9, %rax
	cmp %rax, %rdx
	je do9


	jmp draw_number_fin
do0:
	mov $0xffffff, %rdx
	call draw_9
	mov $0x000000, %rdx
	
	mov %r10, %rcx
	call draw_0
	jmp draw_number_fin
do1:
	mov $0xffffff, %rdx
	call draw_0
	mov $0x000000, %rdx
	mov %r10, %rcx
	call draw_1
	jmp draw_number_fin

do2:
	mov $0xffffff, %rdx
	call draw_1
	mov $0x0fff00, %rdx
	mov %r10, %rcx
	call draw_2
	jmp draw_number_fin

do3:
	mov $0xffffff, %rdx
	call draw_2
	mov $0x000ffff, %rdx
	mov %r10, %rcx
	call draw_3
	jmp draw_number_fin

do4:
	mov $0xffffff, %rdx
	call draw_3
	mov $0x000000, %rdx
	mov %r10, %rcx
	call draw_4
	jmp draw_number_fin

do5:
	mov $0xffffff, %rdx
	call draw_4
	mov $0x000000, %rdx
	mov %r10, %rcx
	call draw_5
	jmp draw_number_fin

do6:
	mov $0xffffff, %rdx
	call draw_5
	mov $0x000000, %rdx
	mov %r10, %rcx
	call draw_6
	jmp draw_number_fin

do7:
	mov $0xffffff, %rdx
	call draw_6
	mov $0x000000, %rdx
	mov %r10, %rcx
	call draw_7

	jmp draw_number_fin


do8:
	mov $0xffffff, %rdx
	call draw_7
	mov $0x000000, %rdx
	mov %r10, %rcx
	call draw_8
	jmp draw_number_fin

do9:
	mov $0xffffff, %rdx
	call draw_8
	mov $0x000000, %rdx
	mov %r10, %rcx
	call draw_9
	jmp draw_number_fin

draw_number_fin:
	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx


	mov $0x1, %rax
	mov %r12, %rdx
	add %rax, %rdx
	mov %rdx, %r12
	
	jmp draw_number

# coord dans rcx, couleur dans rdx
# utilise r8, r9
draw_0:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x501, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x104, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x104, %rdx
	add %r8, %rdx
	call  line_horiz


	mov %r9, %rbx
	mov $0x501, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_horiz

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx

draw_1:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	
	mov $0x104, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_vert

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx

# coord dans rcx
draw_2:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x301, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x304, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x104, %rdx
	add %r8, %rdx
	call  line_horiz
	
	mov %r9, %rbx
	mov $0x301, %rcx
	add %r8, %rcx
	mov $0x304, %rdx
	add %r8, %rdx
	call  line_horiz


	mov %r9, %rbx
	mov $0x501, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_horiz

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx

# coord dans rcx
draw_3:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	
	mov %r9, %rbx
	mov $0x104, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x104, %rdx
	add %r8, %rdx
	call  line_horiz
	
	mov %r9, %rbx
	mov $0x302, %rcx
	add %r8, %rcx
	mov $0x304, %rdx
	add %r8, %rdx
	call  line_horiz


	mov %r9, %rbx
	mov $0x501, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_horiz

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx

# coord dans rcx
draw_4:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	
	mov $0x301, %rcx
	add %r8, %rcx
	mov $0x501, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x104, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x301, %rcx
	add %r8, %rcx
	mov $0x304, %rdx
	add %r8, %rdx
	call  line_horiz

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx


# coord dans rcx
draw_5:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	
	mov $0x104, %rcx
	add %r8, %rcx
	mov $0x304, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x301, %rcx
	add %r8, %rcx
	mov $0x501, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x104, %rdx
	add %r8, %rdx
	call  line_horiz
	
	mov %r9, %rbx
	mov $0x301, %rcx
	add %r8, %rcx
	mov $0x304, %rdx
	add %r8, %rdx
	call  line_horiz


	mov %r9, %rbx
	mov $0x501, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_horiz

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx

# coord dans rcx
draw_6:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x501, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x104, %rcx
	add %r8, %rcx
	mov $0x304, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x104, %rdx
	add %r8, %rdx
	call  line_horiz
	
	mov %r9, %rbx
	mov $0x301, %rcx
	add %r8, %rcx
	mov $0x304, %rdx
	add %r8, %rdx
	call  line_horiz


	mov %r9, %rbx
	mov $0x501, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_horiz

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx

# coord dans rcx
draw_7:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	
	mov $0x401, %rcx
	add %r8, %rcx
	mov $0x501, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x104, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x501, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_horiz

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx


# coord dans rcx
draw_8:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x501, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x104, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x104, %rdx
	add %r8, %rdx
	call  line_horiz
	
	mov %r9, %rbx
	mov $0x301, %rcx
	add %r8, %rcx
	mov $0x304, %rdx
	add %r8, %rdx
	call  line_horiz


	mov %r9, %rbx
	mov $0x501, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_horiz

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx


# coord dans rcx
draw_9:
	mov %rcx, %r8
	mov %rdx, %r9
	mov %r9, %rbx
	
	mov $0x301, %rcx
	add %r8, %rcx
	mov $0x501, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x104, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_vert

	mov %r9, %rbx
	mov $0x101, %rcx
	add %r8, %rcx
	mov $0x104, %rdx
	add %r8, %rdx
	call  line_horiz
	
	mov %r9, %rbx
	mov $0x301, %rcx
	add %r8, %rcx
	mov $0x304, %rdx
	add %r8, %rdx
	call  line_horiz


	mov %r9, %rbx
	mov $0x501, %rcx
	add %r8, %rcx
	mov $0x504, %rdx
	add %r8, %rdx
	call  line_horiz

	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx


bottom_line:
	mov $0xf00, %rcx
	mov $0x00ffff, %rax

	mov $0x1, %rbx
	mov $0xf0f, %rdx
bottom_line_boucle:
	cmp %rdx, %rcx
	mov %rax, (%rcx)
	add %rbx, %rcx
	je bottom_line_out
	jmp bottom_line_boucle
bottom_line_out:
	
	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx

# from %rcx to %rdx, couleur dans %rbx
line_vert:
	mov $0x10000, %rax
	add %rax, %rcx
	add %rax, %rdx
	
	mov %rbx, %rax

	mov $0x100, %rbx
line_vert_boucle:
	cmp %rdx, %rcx
	mov %rax, (%rcx)
	add %rbx, %rcx
	je line_vert_out
	jmp line_vert_boucle
line_vert_out:
	
	mov (%rsp), %rcx
	mov $0x8, %rbx
	add %rbx, %rsp
	jmp *%rcx

# from %rcx to %rdx, couleur dans %rbx
line_horiz:
	mov $0x10000, %rax
	add %rax, %rcx
	add %rax, %rdx
	
	mov %rbx, %rax

	mov $0x1, %rbx
	# cette arnaque :
	jmp line_vert_boucle
