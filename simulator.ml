open Netlist_ast

type int_ident = int

type int_value =
	| IVBit of bool
	| IVBitArray of bool array
	| IVRam of bool array * bool array

type int_arg =
    | IAvar of int_ident
    | IAconst of int_value

type int_exp =
    | IEarg of int_arg
    | IEreg of int_ident 
    | IEnot of int_arg
    | IEbinop of binop * int_arg * int_arg
    | IEmux of int_arg * int_arg * int_arg
    | IErom of int (*addr size*) * int (*word size*) * int_arg (*read_addr*) * int_ident
    | IEwritetoram of int (*addr size*) * int (*word size*)
        * int_arg (*write_enable*)
        * int_arg (*write_addr*) * int_arg (*data*) * int_ident
    | IEconcat of int_arg * int_arg
    | IEslice of int * int * int_arg
    | IEselect of int * int_arg

type int_equation = int_ident * int_exp

type int_env = (int_value array)

type int_program =
    { p_eqs : int_equation list;
      p_inputs : int_ident list;
      p_outputs : int_ident list;
	  nmax : int;
	  p_vars : ty array; }

module SMap = Map.Make(String)

exception Not_an_array
exception Not_a_bit
exception Array_too_small
exception Indices_problem

let program_to_int (p:program) =
	let nmax = (List.length Netlist_ast.(p.p_eqs)) + (List.length Netlist_ast.(p.p_inputs)) in
	let ctx = Array.make nmax TBit in
	let corres = ref SMap.empty in
	let current_var = ref 0 in
	let get_int_from_var v =
		begin try SMap.find v !corres
		with | Not_found -> (let id = !current_var in
		corres := SMap.add v id (!corres); incr current_var; id) end
	in
	let ident_to_int = get_int_from_var in
	let value_to_int = function
		| VBitArray(a) -> IVBitArray(a)
		| VBit(a) -> IVBit(a)
	in
	let arg_to_int = function
		| Avar(i) -> IAvar(ident_to_int i)
		| Aconst(c) -> IAconst(value_to_int c)
	in
	let rec expr_to_int id = function
		| Earg(a) -> IEarg(arg_to_int a)
		| Ereg(i) -> IEreg(ident_to_int i)
		| Enot(a) -> IEnot(arg_to_int a)
		| Ebinop(op, a1, a2) -> IEbinop(op, arg_to_int a1, arg_to_int a2)
		| Emux(a1, a2, a3) -> IEmux(arg_to_int a1, arg_to_int a2, arg_to_int a3)
		| Erom(int1, int2, a) -> (ctx.(id) <- TRam (int1, int2); IErom(int1, int2, arg_to_int a, id))
		| Eram(int1, int2, a, a2, a3, a4) -> (ctx.(id) <- TRam(int1, int2); IErom(int1, int2, arg_to_int a, id))
		| Ewritetoram(int1, int2, a2, a3, a4, ident) -> (IEwritetoram(int1, int2, arg_to_int a2, arg_to_int a3, arg_to_int a4, get_int_from_var ident))
		| Econcat(a, a2) -> IEconcat(arg_to_int a, arg_to_int a2)
		| Eslice(int1, int2, a) -> IEslice(int1,int2, arg_to_int a)
		| Eselect(int1, a) -> IEselect(int1, arg_to_int a)
	in

	let eq_to_int (i, e) = (ident_to_int i, expr_to_int (ident_to_int i) e)
	in
	let list_vars_to_int = List.map get_int_from_var
	in
	(* First let's number the inputs *)
	List.iter (fun i -> let _ = get_int_from_var i in ()) Netlist_ast.(p.p_inputs);
	(* Then the outputs *)
	List.iter (fun i -> let _ = get_int_from_var i in ()) Netlist_ast.(p.p_outputs);
	Env.iter (fun k i -> ctx.(get_int_from_var k) <- i) Netlist_ast.(p.p_vars);
	
	let int_program = { p_inputs = list_vars_to_int Netlist_ast.(p.p_inputs); p_eqs = List.map eq_to_int Netlist_ast.(p.p_eqs); p_outputs = list_vars_to_int Netlist_ast.(p.p_outputs); p_vars = ctx; nmax = nmax;} in

	print_string "int_program computed, now the simulation\n";

	nmax, int_program

let print_var = function
	| IVBit i -> begin Format.printf "%d@." (if i then 1 else 0) end
	| IVRam(a, _) | IVBitArray a -> (
		for i = 0 to (Array.length a)/4 - 1 do
			let j = (if a.(i*4) then 8 else 0) + (if a.(i*4+1) then 4 else 0) + (if a.(i*4+2) then 2 else 0) + (if a.(i*4+3) then 1 else 0) in
			if j <> 0 then
				Format.printf "%x" j
			else 
				Format.printf "0"
		done;
		Format.printf "@.";)

let print_tab p =
	Array.iteri (fun k i -> print_int k; print_string ": "; print_var i; print_string " ") p; print_string "\n"



let compute (expr:int_exp) (values:int_env) =
	let get_val = function
		| IAvar(i) -> values.(i)
		| IAconst(c) -> c
	in
	let get_array = function
		| IVBitArray(a) -> a
		| IVBit(v) -> Array.make 1 v
		| IVRam(a,_) -> a
	in
	let get_bit = function
		| IVBit(a) -> a
		| _ -> raise Not_a_bit
	in
	(* it is made sure in scheduler.ml that i has not been computed yet *)
	let get_reg i = values.(i)
	in
	let or_val val1 val2 = match val1, val2 with
		| IVBit v1, IVBit v2 -> IVBit (v1 || v2)
		| _ -> failwith "not implemented"
	in
	let and_val val1 val2 = match val1, val2 with
		| IVBit v1, IVBit v2 -> IVBit (v1 && v2)
		| _ -> failwith "not implemented"
	in
	let not_val val1 = match val1 with
		| IVBit v2 -> IVBit (not v2)
		| _ -> failwith "not implemented"
	in
	match expr with
	| IEarg(a) -> get_val a
	| IEreg(i) -> get_reg i
	| IEnot(a) -> not_val (get_val a)
	| IEbinop(op, a1, a2) -> begin
		let i1 = get_val a1 and i2 = get_val a2 in
		match op with
		| Or -> or_val i1 i2
		| And -> and_val i1 i2
		| Nand -> or_val (not_val i1) (not_val i2)
		| Xor -> or_val (and_val i1 (not_val i2)) (and_val (not_val i1) i2)
		end
	| IEmux(a3, a2, a1) ->
		(* FIXME: pas sûr que ça soit le bon sens *)
		let t1 = get_bit (get_val a1) in
		if t1 then get_val a2 else get_val a3
	| IErom(int1, int2, a, i_rom) ->
		begin
		let t2 = get_array (get_val a) in
		let IVRam(current_val, memory) = values.(i_rom) in
		let addr = 8 * (Array.fold_left (fun c b -> 2*c + (if b then 1 else 0)) 0 t2) in
		(* for i = 0 to (Array.length memory)-1 do if memory.(i) then print_int 1 else print_int 0; done; print_string "\n"; *)
		Array.blit memory addr current_val 0 int2;
		IVRam(current_val, memory)
		end
    | IEwritetoram(_, word_size, write_enable, write_addr, data, i_rom) ->
		begin
		let i1  = get_bit (get_val write_enable) in
		if i1 then
		begin
			let t2 = get_array (get_val write_addr) in
			let real_data = get_array (get_val data) in
			let IVRam(current_val, memory) = values.(i_rom) in
			let addr = 8 * (Array.fold_left (fun c b -> 2*c + (if b then 1 else 0)) 0 t2) in
			Array.blit real_data 0 memory addr word_size;
		end;
			IVBit true
		end
	| IEconcat(a, a2) ->
		let bit_array1 = get_array (get_val a) in
		let bit_array2 = get_array (get_val a2) in
		IVBitArray (Array.concat [bit_array1;bit_array2])
	| IEslice(int1, int2, a) ->
		let bit_array = get_array (get_val a) in
		if int1 >= 0 && int1 <= int2 && int2 <= Array.length bit_array then
			IVBitArray (Array.sub bit_array int1 (int2 - int1 + 1))
		else raise Indices_problem
	| IEselect(int1, a) ->
		let bit_array = get_array (get_val a) in
		if int1 < Array.length bit_array then IVBit(bit_array.(int1))
		else raise Array_too_small


let update_value (c:int_env) (p:int_program) =
	List.iter (fun (var_ident, expr) ->
		c.(var_ident) <- compute expr c;
	) p.p_eqs


let build_from_program = program_to_int

