open Netlist_ast
open Graph

exception Combinational_cycle

let read_exp inputs g liste_eqs (i, expr) =
	let read_arg = function
		| Avar(id) -> [id]
		| _ -> []
	in
	let read_arg = function
		| Avar(id) -> [id]
		| _ -> []
	in
	let read_expr = function
    | Earg(arg) -> read_arg arg
    | Ereg(id) ->
		begin
		if List.mem id inputs then
			(add_node g ("input~"^id);
			add_edge g i ("input~"^id);
			liste_eqs := List.map (fun (eq_i, eq_expr) -> if eq_i = i then (eq_i, Earg (Avar ("input~"^id)))
			else (eq_i, eq_expr)) !liste_eqs;
			liste_eqs := ("input~"^id, Earg (Avar(id)))::(!liste_eqs)
			)
		else (
			add_node g id;
			add_edge g i id;)
		;
		[]
		end
	| Erom(_, _, a) -> read_arg a
	| Eram(size, word, a1, a2, a3, a4) ->
		begin
			add_node g ("write~"^i);
			List.iter (fun x -> add_node g x; add_edge g x ("write~"^i)) (read_arg a2 @ read_arg a3 @ read_arg a4 @ [i]);
			liste_eqs := ("write~"^i, Ewritetoram(size, word, a2, a3, a4, i))::!liste_eqs;
			read_arg a1
		end
    | Enot(e) -> read_arg e
    | Ebinop(_, a, b) -> (read_arg a) @ (read_arg b)
    | Emux(a, b, c) -> (read_arg a) @ (read_arg b) @ (read_arg c)
    | Econcat(a, b) -> (read_arg a) @ (read_arg b)
    | Eslice(_, _, a) -> read_arg a
    | Eselect(_, a) -> read_arg a

	in read_expr expr

let log s = 
		Format.printf "%f %s\n@." (Sys.time ()) s

let schedule p =
	(* construction du graphe *)
	let g = mk_graph () in
	let liste_eqs = ref p.p_eqs in
	log "Start creating the graph";
	List.iter (fun eq ->
		let (dest, _) = eq in
		let dest_node = add_node g dest in
		let args = read_exp p.p_inputs g liste_eqs eq in
		List.iter (fun n ->
				let n_node = add_node g n in
				add_edge_real g n_node dest_node) args;
	) p.p_eqs;
	log "Equation graph created";

	if has_cycle g then raise Combinational_cycle
	else
		begin
		log "No cycle in graph";
		let ident_list = topological g in
		log "Topological sort done";
		(* code sale, il faudrait utiliser une hashtbl tout le long, mais bon… *)
		let eqs_tbl = Hashtbl.create 50000 in
		List.iter (fun (s, e) -> Hashtbl.replace eqs_tbl s (s,e)) !liste_eqs;
		log "Hashtbl built";
		let eq_list = List.fold_right (fun i l -> try
			let eq = Hashtbl.find eqs_tbl i in
			eq::l
			with Not_found -> l) ident_list [] in
		{ p with p_eqs = eq_list }
		end

