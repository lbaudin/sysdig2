open Simulator

let print_only = ref false
let number_steps = ref (-1)
let rom_filename = ref ""
let no_input = ref false
let frequency = ref 1000

exception Number_of_steps_missing

let load_rom n = function
	| "" -> Array.make n true
	| path -> begin
		let in_channel = open_in_bin path in
		let ret = Array.make n true in
		let count = ref 0 in
		let eof = ref false in
		while !count < n-8 do
			try begin
				let byte = ref (input_byte in_channel) in
				let to_bool = function
					| 0 -> false
					| 1 -> true
					| _ -> failwith "not binary"
				in
				for i = 7 downto 0 do
					ret.(!count + i) <- to_bool (!byte mod 2);
					byte := !byte/2;
				done;
				count := !count + 8;
			end with
				| End_of_file -> (eof := true; count := n;)
		done;
		if not !eof then print_string "ROM file too large.\n";
		ret
		end

let log s = 
		Format.printf "%f %s\n@." (Sys.time ()) s
		
		
let compile filename =
  try
	  begin
	  	log "Start netlist reading\n";
		let prog = Netlist.read_file filename in
		log "Netlist read";
		let prog_scheduled = Scheduler.schedule prog in
		log "Netlist scheduled\n";
		(*let _ = Netlist_printer.print_program stdout prog_scheduled in *)
		let n_var, prog_sim = Simulator.build_from_program prog_scheduled in
		log "Converted to int";
		let n_inputs = List.length (Simulator.(prog_sim.p_inputs)) in
		let inputs = if !no_input then
				Input_read.fake_inputs n_inputs prog_sim.p_vars (max !number_steps 1)
			else
				Input_read.load_inputs n_inputs prog_sim.p_vars (max !number_steps 1) in
		let env = Array.init n_var (fun i -> match prog_sim.p_vars.(i) with
			| Netlist_ast.TBit -> Simulator.IVBit true
			| Netlist_ast.TBitArray(s) -> Simulator.IVBitArray (Array.make s true) 
			| Netlist_ast.TRam(addr, word) -> Simulator.IVRam(Array.make word true, (load_rom (word*(Int32.(to_int (shift_left one addr))))  !rom_filename))
		)
		in
		log "Env created, ram left";
		print_string "-\n";
		if !number_steps > 0 then
		List.iter (fun input ->
			let start_time = Sys.time () in
			Array.blit input 0 env 0 n_inputs;
			Simulator.update_value env prog_sim;
			let s = ref n_inputs in
			List.iter (fun name -> Simulator.print_var env.(!s); incr s) Netlist_ast.(prog_scheduled.p_outputs); print_string "-\n";
			while -.start_time +. Sys.time () < 1. /. (float_of_int !frequency) do
				();
			done;)
		inputs
		else
			begin
			while true do
				let start_time = Sys.time () in
				Array.blit (List.hd inputs) 0 env 0 n_inputs;
				Simulator.update_value env prog_sim;
				let s = ref n_inputs in
				List.iter (fun name -> Simulator.print_var env.(!s); incr s) Netlist_ast.(prog_scheduled.p_outputs); print_string "-\n";
				while -.start_time +. Sys.time () < 1. /. (float_of_int !frequency) do
					();
				done
			done
			end
	  end
  with
    | Netlist.Parse_error s -> Format.eprintf "An error accurred: %s@." s; exit 2
	| Scheduler.Combinational_cycle ->
		Format.eprintf "The netlist has a combinatory cycle.@.";
		exit 2

let main () =
  Arg.parse
     ["-n", Arg.Set_int number_steps, "Number of steps to simulate";
	  "-noinput", Arg.Set no_input, "Set every input to 0 for every cycle";
	  "-frequency", Arg.Set_int frequency, "Frequency";
	  "-r", Arg.Set_string rom_filename, "File of the ROM"]
    compile
    ""
;;

main ()
