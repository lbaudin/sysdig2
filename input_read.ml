exception Invalid_input

let load_chunk input_size str ty =
		let r = ref 0 in
		let t = Array.init input_size (fun i ->
			if !r >= String.length str then raise Invalid_input
			else
			match ty.(i) with
			| Netlist_ast.TBit ->
				(let v = Simulator.IVBit (str.[!r] = '1') in
				incr r; v)
			| Netlist_ast.TBitArray(s) -> 
				Simulator.IVBitArray (Array.init s (fun i ->
					let v = (str.[!r] = '1') in
					incr r; v))
			)
		in
		if !r <> String.length str then raise Invalid_input else t
				
let load_fake_chunk input_size ty =
		let t = Array.init input_size (fun i ->
			match ty.(i) with
			| Netlist_ast.TBit ->
				Simulator.IVBit true
			| Netlist_ast.TBitArray(s) -> 
				Simulator.IVBitArray (Array.make s false)
			)
		in t
				


let rec load_inputs input_size ty = function
	| 0 -> []
	| n -> let s = Pervasives.read_line () in
		(load_chunk input_size s ty: Simulator.int_value array)::(load_inputs input_size ty (n-1))
	
let rec fake_inputs input_size ty = function
	| 0 -> []
	| n -> (load_fake_chunk input_size ty: Simulator.int_value array)::(fake_inputs input_size ty (n-1))
	
